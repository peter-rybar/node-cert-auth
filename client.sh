#! /bin/bash

echo "valid --------------------------------------------------------"
# curl -v -s -k --key certs/client1-key.pem --cert certs/client1-crt.pem https://localhost:4433
curl -s -k --key certs/client1-key.pem --cert certs/client1-crt.pem https://localhost:4433

echo "revoked ------------------------------------------------------"
# curl -v -s -k --key certs/client2-key.pem --cert certs/client2-crt.pem https://localhost:4433
curl -s -k --key certs/client2-key.pem --cert certs/client2-crt.pem https://localhost:4433
