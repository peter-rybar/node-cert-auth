import * as express from "express";
import * as bodyParser from "body-parser";


function clientCertAuth() {
    return (req, res, next) => {
        console.log("client cert auth:", req.client.authorized);
        if (!req.client.authorized) {
            return res.status(401).send("User is not authorized");
        }
        // examine the cert itself, and even validate based on that!
        const cert = req.socket.getPeerCertificate();
        if (cert.subject) {
            console.log("cert:", cert.subject.CN, cert.subject.emailAddress);
            // console.log("cert:", cert);
        }
        next();
    };
}

export const app: express.Application = express();
app.use(clientCertAuth());
app.use(bodyParser.json({ type: "*/*" }));
app.use((req, res, next) => {
    res.writeHead(200);
    const body = JSON.stringify(req.body);
    console.log("body", body);
    res.end(body);
    next();
});
