/// <reference path="../node_modules/@types/jest/index.d.ts" />
import * as https from "https";
import * as fs from "fs";
import * as request from "request-promise-native";
import { join } from "path";
import { app } from "./app";

describe("Client", () => {

    let server;

    beforeAll(async done => {
        console.log("server listen");
        const options = {
            key: fs.readFileSync(join(__dirname, "../certs/server-key.pem")),
            cert: fs.readFileSync(join(__dirname, "../certs/server-crt.pem")),
            ca: fs.readFileSync(join(__dirname, "../certs/ca-crt.pem")),
            crl: fs.readFileSync(join(__dirname, "../certs/ca-crl.pem")),
            requestCert: true,
            rejectUnauthorized: false
        };
        server = https
            .createServer(options, app)
            .listen(4433, () => {
                const host = (server.address() as any).address;
                const port = (server.address() as any).port;
                console.log(`Express HTTPS server: host '${host}' port ${port}`);
                done();
            });
    });

    afterAll(() => {
        console.log("server close");
        server.close();
    });

    describe("Auth", () => {

        test("request - client1", async done => {
            const client = "client1";
            const options = {
                hostname: "localhost",
                port: 4433,
                path: "/",
                method: "GET",
                key: fs.readFileSync(join(__dirname, `../certs/${client}-key.pem`)),
                cert: fs.readFileSync(join(__dirname, `../certs/${client}-crt.pem`))
                // passphrase: "password"
            };

            const req = https.request(options, res => {
                const body = [];
                res.on("data", data => {
                    body.push(data);
                });
                res.on("end", () => {
                    // console.log(res.statusCode, body.join(""));
                    expect(res.statusCode).toBe(200);
                    expect(body.join("")).toBe("{}");
                    done();
                });
            });
            req.on("error", err => {
                console.log(err);
                done();
            });
            req.end();
        });

        test("request - client2", async done => {
            const client = "client2";
            const options = {
                hostname: "localhost",
                port: 4433,
                path: "/",
                method: "GET",
                key: fs.readFileSync(join(__dirname, `../certs/${client}-key.pem`)),
                cert: fs.readFileSync(join(__dirname, `../certs/${client}-crt.pem`))
                // passphrase: "password"
            };

            const req = https.request(options, res => {
                const body = [];
                res.on("data", data => {
                    body.push(data);
                });
                res.on("end", () => {
                    // console.log(res.statusCode, body.join(""));
                    expect(res.statusCode).toBe(401);
                    expect(body.join("")).toBe("User is not authorized");
                    done();
                });
            });
            req.on("error", err => {
                console.log(err);
                done();
            });
            req.end();
        });


        test("request-promise - client1", async () => {
            const client = "client1";
            return request
                .get("https://localhost:4433/", {
                    resolveWithFullResponse: true,
                    key: fs.readFileSync(`certs/${client}-key.pem`),
                    cert: fs.readFileSync(`certs/${client}-crt.pem`)
                    // passphrase: "password"
                })
                .then(res => {
                    // console.log(res.statusCode, res.body);
                    expect(res.statusCode).toBe(200);
                    expect(res.body).toBe("{}");
                });
        });

        test("request-promise - client2", async () => {
            const client = "client2";
            return request
                .get("https://localhost:4433/", {
                    resolveWithFullResponse: true,
                    key: fs.readFileSync(`certs/${client}-key.pem`),
                    cert: fs.readFileSync(`certs/${client}-crt.pem`)
                    // passphrase: "password"
                })
                .then(res => {
                    // console.log(res.statusCode, res.body);
                    expect(res.statusCode).toBe(401);
                    expect(res.body).toBe("User is not authorized");
                })
                .catch(err => {
                    // console.error("error", err);
                    expect(err.statusCode).toBe(401);
                    expect(err.error).toBe("User is not authorized");
                });
        });

    });

});
