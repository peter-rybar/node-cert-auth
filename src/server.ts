import * as fs from "fs";
import * as http from "http";
import * as https from "https";
import { join } from "path";
import { app } from "./app";

const options = {
    key: fs.readFileSync(join(__dirname, "../certs/server-key.pem")),
    cert: fs.readFileSync(join(__dirname, "../certs/server-crt.pem")),
    ca: fs.readFileSync(join(__dirname, "../certs/ca-crt.pem")),
    crl: fs.readFileSync(join(__dirname, "../certs/ca-crl.pem")),
    requestCert: true,
    rejectUnauthorized: false
};

export const httpsServer = https
    .createServer(options, app)
    .listen(4433, () => {
        const host = (httpsServer.address() as any).address;
        const port = (httpsServer.address() as any).port;
        console.log(`HTTPS server: host '${host}' port ${port}`);
    });

export const httpServer = http
    .createServer(app)
    .listen(8080, () => {
        const host = (httpServer.address() as any).address;
        const port = (httpServer.address() as any).port;
        console.log(`HTTP server: host '${host}' port ${port}`);
    });
