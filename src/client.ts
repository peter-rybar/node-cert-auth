import * as http from "http";
import * as https from "https";
import * as fs from "fs";
import * as request from "request-promise-native";
import { join } from "path";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const client = "client1";
// const client = "client2";

// const options = {
//     hostname: "localhost",
//     port: 4433,
//     path: "/",
//     method: "GET",
//     key: fs.readFileSync(join(__dirname, `../certs/${client}-key.pem`)),
//     cert: fs.readFileSync(join(__dirname, `../certs/${client}-crt.pem`))
//     // passphrase: "password"
// };

const options = {
    hostname: "localhost",
    port: 4433,
    // port: 8080,
    path: "/topics/rest_api_v1_input",
    method: "POST",
    headers: {
        "Content-Type": "application/vnd.kafka.json.v2+json",
        Accept: "application/vnd.kafka.v2+json"
    },
    // family: 4,
    resolveWithFullResponse: true,
    key: fs.readFileSync(join(__dirname, `../certs/${client}-key.pem`)),
    cert: fs.readFileSync(join(__dirname, `../certs/${client}-crt.pem`)),
    ecdhCurve: "auto"
};

const req = https.request(options,
    res => {
        const body = [];
        res.on("data", data => {
            body.push(data);
        });
        res.on("end", () => {
            console.log(res.statusCode, body.join(""));
        });
    });
req.on("error", err => {
    console.log(err);
});
req.write(JSON.stringify({ records: [{ value: { sessionId: "1" } }] }));
req.end();

request
    .post("https://localhost:4433/", {
        json: { records: [{ value: { sessionId: "1" } }] },
        // body: `JSON.stringify({ records: [{ value: { sessionId: "1" } }] })`,
        resolveWithFullResponse: true,
        key: fs.readFileSync(join(__dirname, `../certs/${client}-key.pem`)),
        cert: fs.readFileSync(join(__dirname, `../certs/${client}-crt.pem`))
        // ca: fs.readFileSync(join(__dirname, `../certs/ca-crt.pem`))
        // passphrase: "password"
    })
    .then(res => {
        console.log(res.statusCode, JSON.stringify(res.body));
    })
    .catch(err => {
        console.error("error", err);
    });
